package org.home.djm.nrollingdice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

public class StartRolling extends Activity {

	private ImageView mDice;
	private EditText mMaxRollsNo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_rolling);

		mDice = (ImageView) findViewById(R.id.dice);
		mMaxRollsNo = (EditText) findViewById(R.id.noOfRolls);

		mDice.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				int maxRollNo = Integer.parseInt(mMaxRollsNo.getText().toString());
				Intent myIntent = new Intent(StartRolling.this, RollingDice.class);
				myIntent.putExtra("MAX_ROLL_NO", maxRollNo);
				startActivity(myIntent);
			}
		});
	}
}
