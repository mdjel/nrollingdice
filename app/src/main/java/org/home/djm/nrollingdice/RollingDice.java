package org.home.djm.nrollingdice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class RollingDice extends Activity {

	private ImageView mDice;
	private TextView mRolls;

	private int mMaxRollsNo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rolling_dice);

		Intent mIntent = getIntent();
		mMaxRollsNo = mIntent.getIntExtra("MAX_ROLL_NO", 0);

		mDice = (ImageView) findViewById(R.id.dice);
		mRolls = (TextView) findViewById(R.id.rolls);
		mRolls.setText(Integer.toString(mMaxRollsNo));

		mDice.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Random random = new Random();
				int randomInt = random.nextInt(mMaxRollsNo) + 1;
				mRolls.setText(Integer.toString(randomInt));
			}
		});
	}
}
